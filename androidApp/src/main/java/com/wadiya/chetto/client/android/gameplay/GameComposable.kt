package com.wadiya.chetto.client.android

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.wadiya.chetto.client.android.gameplay.BoardComposable
import com.wadiya.chetto.client.android.gameplay.ChatComposable
import com.wadiya.chetto.client.android.gameplay.GameStatusComposable
import com.wadiya.chetto.engine.boards.Board2D
import com.wadiya.chetto.engine.gamemodes.ClassicChess

val board = Board2D().apply {
    loadDefaultStartingPosition()
}
val game = ClassicChess(board)

@ExperimentalMaterialApi
@Composable
fun Game(navController: NavHostController) {
    var openDialog by remember { mutableStateOf(false) }
    if (openDialog) {
        AlertDialog(
            onDismissRequest = { openDialog = false },
            title = {
                Text("Exit Game")
            },
            text = {
                Text("Are you sure you want to exit the game?")
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        navController.navigateUp()
                    }
                ) {
                    Text("Exit")
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        openDialog = false
                    }
                ) {
                    Text("Back to game")
                }
            },
        )
    }

    BackHandler(enabled = true) {
        openDialog = true
    }

    Box(
        Modifier.fillMaxSize()
    ) {
        BoardComposable(game)
        Column(Modifier.fillMaxSize()) {
            GameStatusComposable(game)
            Spacer(Modifier.weight(1f))
            ChatComposable()
        }
    }
}
