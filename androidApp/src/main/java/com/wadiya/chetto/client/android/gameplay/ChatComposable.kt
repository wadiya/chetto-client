package com.wadiya.chetto.client.android.gameplay

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuDefaults.outlinedTextFieldColors
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import com.google.accompanist.insets.navigationBarsWithImePadding

@Composable
@Preview
@ExperimentalMaterialApi
fun ChatComposable() {
    val chatBarHeight = 50.dp
    val background = Color.Black.copy(0.8f)
    var pendingText by rememberSaveable { mutableStateOf("") }
    Box(
        Modifier
            .fillMaxWidth()
            .background(
                Brush.verticalGradient(
                    listOf(Color.Transparent, background)
                )
            )
            .navigationBarsWithImePadding()
            .padding(vertical = 8.dp, horizontal = 16.dp)
            .padding(top = 60.dp)
    ) {
        Row(Modifier.padding(8.dp)) {
            OutlinedTextField(
                modifier = Modifier
                    .height(chatBarHeight)
                    .weight(1f),
                value = pendingText,
                onValueChange = { pendingText = it },
                singleLine = true,
                placeholder = { Text("Send message", color = Color.White, fontSize = 3.em) },
                shape = RoundedCornerShape(100),
                colors = outlinedTextFieldColors(
                    textColor = Color.White,
                    unfocusedBorderColor = Color.Gray,
                    focusedBorderColor = Color.Gray,
                    cursorColor = Color.White
                )
            )
            IconButton(onClick = { /*TODO*/ },
                Modifier.height(chatBarHeight)
                    .padding(start = 16.dp)
            ) {
                Icon(
                    Icons.Filled.Send, "Send",
                    tint = Color.White,
                    modifier = Modifier.height(32.dp)
                        .aspectRatio(1f)
                )
            }
        }
    }
}