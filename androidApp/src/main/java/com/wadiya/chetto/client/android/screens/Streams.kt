package com.wadiya.chetto.client.android.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.google.accompanist.placeholder.PlaceholderHighlight
import com.google.accompanist.placeholder.placeholder
import com.google.accompanist.placeholder.shimmer
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState


@Composable
fun Streams() {
    val placeholder = Modifier.placeholder(
        visible = true,
        color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.3f),
        highlight = PlaceholderHighlight.shimmer(
            MaterialTheme.colorScheme.onBackground.copy(alpha = 0.2f)
        )
    )

    val isRefreshing = rememberSwipeRefreshState(true)

    SwipeRefresh(
        state = isRefreshing,
        onRefresh = { }
    ) {
        LazyColumn(Modifier.fillMaxSize()) {
            items(1000) { i ->
                Row(
                    Modifier.padding(12.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Surface(
                        placeholder
                            .width(80.dp)
                            .height(80.dp),
                        shape = RoundedCornerShape(12.dp)
                    ) { }
                    Spacer(Modifier.width(8.dp))
                    Text(
                        "",
                        modifier = placeholder
                            .width(200.dp)
                    )
                }
            }
        }
    }
}