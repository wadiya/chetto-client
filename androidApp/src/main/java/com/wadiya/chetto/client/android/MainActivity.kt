package com.wadiya.chetto.client.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.wadiya.chetto.client.android.screens.*
import com.example.compose.AppTheme
import com.google.accompanist.insets.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.launch

sealed class Screen(val route: String, val name: String, val navIcon: ImageVector) {
    object GameModes : Screen("gameModes", "Play", Icons.Filled.SportsEsports)
    object Streams : Screen("streams", "Watch", Icons.Filled.Public)
    object Profile : Screen("profile", "Profile", Icons.Filled.Person)
    object Store : Screen("store", "Store", Icons.Filled.ShoppingCart)

    object MyGames : Screen("myGames", "My Games", Icons.Filled.List)
    object Settings : Screen("settings", "Settings", Icons.Filled.Settings)
}

val navItems = listOf(
    Screen.GameModes, Screen.Streams, Screen.Profile, Screen.Store
)

val drawerItems = listOf(
    Screen.MyGames, Screen.Settings
)

@ExperimentalFoundationApi
@ExperimentalMaterial3Api
@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            AppTheme {
                val navController = rememberNavController()
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentDestination = navBackStackEntry?.destination
                val currentScreen = navItems.firstOrNull { screen ->
                    currentDestination?.hierarchy?.any {
                        it.route?.startsWith(screen.route) ?: false
                    } == true
                }
                val scaffoldState = rememberScaffoldState()

                val systemUiController = rememberSystemUiController()
                val useDarkIcons = !isSystemInDarkTheme()
                SideEffect {
                    systemUiController.setSystemBarsColor(
                        color = Color.Transparent,
                        darkIcons = useDarkIcons
                    )
                }

                var isFullscreen by remember { mutableStateOf(false) }

                @Composable
                fun setFullscreen(fullscreen: Boolean) {
                    isFullscreen = fullscreen
                    SideEffect {
                        systemUiController.setSystemBarsColor(
                            color = Color.Transparent,
                            darkIcons = if(fullscreen) false else useDarkIcons
                        )
                    }
                }

                ProvideWindowInsets {
                    Scaffold(
                        scaffoldState = scaffoldState,
                        topBar = {
                            AnimatedVisibility(
                                !isFullscreen,
                                enter = expandVertically(),
                                exit = shrinkVertically(),
                            ) {
                                TopAppBar(navController, currentScreen, scaffoldState)
                            }
                        },
                        bottomBar = {
                            AnimatedVisibility(
                                !isFullscreen,
                                enter = expandVertically(expandFrom = Alignment.Top),
                                exit = shrinkVertically(shrinkTowards = Alignment.Top),
                            ) {
                                NavBar(navController, currentScreen)
                            }
                        },
                        drawerContent = { Drawer(currentScreen) },
                        drawerGesturesEnabled = !isFullscreen
                    ) { innerPadding ->
                        NavHost(
                            navController = navController,
                            startDestination = "gameModes",
                            modifier = Modifier.padding(innerPadding)
                        ) {
                            composable(Screen.GameModes.route) {
                                setFullscreen(false)
                                GameModes(navController)
                            }
                            composable("${Screen.GameModes.route}/{gameMode}") {
                                setFullscreen(true)
                                when (it.arguments?.getString("gameMode")) {
                                    GameMode.Classic.name -> GameMode.Classic
                                    GameMode.Variations.name -> GameMode.Variations
                                    GameMode.Arcade.name -> GameMode.Arcade
                                    GameMode.CoopChallenges.name -> GameMode.CoopChallenges
                                    GameMode.BattleRoyale.name -> GameMode.BattleRoyale
                                    else -> throw(Exception("Mode not found"))
                                }.GameView(navController)
                            }
                            composable(Screen.Streams.route) {
                                setFullscreen(false)
                                Streams()
                            }
                            composable(Screen.Profile.route) {
                                setFullscreen(false)
                                Profile()
                            }
                            composable(Screen.Store.route) {
                                setFullscreen(false)
                                Store()
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun NavBar(navController: NavHostController, currentScreen: Screen?) {
    NavigationBar(
        Modifier.navigationBarsPadding(),
        tonalElevation = 8.dp
    ) {
        navItems.forEach { screen ->
            NavigationBarItem(
                icon = { Icon(screen.navIcon, screen.name) },
                label = { Text(screen.name) },
                selected = currentScreen == screen,
                onClick = {
                    navController.navigate(screen.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                }
            )
        }
    }
}

@ExperimentalMaterial3Api
@Composable
private fun TopAppBar(navController: NavHostController, currentScreen: Screen?, scaffoldState: ScaffoldState) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val coroutineScope = rememberCoroutineScope()

    SmallTopAppBar(
        title = { Text(currentScreen?.name ?: "Chetto") },
        modifier = Modifier.statusBarsPadding(),
        navigationIcon = {
            if(currentDestination?.route != currentScreen?.route) {
                IconButton(onClick = { navController.navigateUp() }) {
                    Icon(Icons.Filled.ArrowBack, "Back")
                }
            } else {
                IconButton(onClick = {
                    coroutineScope.launch {
                        scaffoldState.drawerState.open()
                    }
                }) {
                    Icon(Icons.Filled.Menu, "Back")
                }
            }
        },
        actions = {
            TextButton(
                onClick = {
                    navController.navigate("profile") {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                }
            ) {
                Column(
                    horizontalAlignment = Alignment.End
                ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Surface(
                            shape = RoundedCornerShape(12.dp),
                            color = MaterialTheme.colorScheme.primaryContainer
                        ) {
                            Text("13", Modifier.padding(vertical = 2.dp, horizontal = 8.dp))
                        }
                        Spacer(Modifier.width(6.dp))
                        Text(
                            "User5819",
                        )
                    }
                    Spacer(Modifier.height(6.dp))
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Icon(Icons.Filled.AttachMoney, "Money")
                        Text("1302")
                        Spacer(Modifier.width(12.dp))
                        Icon(Icons.Filled.Savings, "Savings")
                        Text("32")
                    }
                }
            }
        }
    )
}

@Preview
@Composable
private fun prevDrawer() = Drawer(null)

@Composable
private fun Drawer(currentScreen: Screen?) {
    Column(
        Modifier.fillMaxSize()
            .statusBarsPadding()
            .padding(12.dp)
    ) {
        Text(
            "Chetto",
            Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.headlineLarge
        )
        Text(
            "Slogan Here",
            Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.headlineSmall
        )
        Spacer(Modifier.height(28.dp))
        drawerItems.forEach { screen ->
            val isSelected = screen == currentScreen
            TextButton(
                modifier = Modifier.fillMaxWidth()
                    .height(56.dp),
//                    .background(if(isSelected) { Color.Transparent } else { MaterialTheme.colorScheme.surface }),
                shape = RoundedCornerShape(28.dp),
                onClick = { }
            ) {
                Icon(screen.navIcon, screen.name)
                Text(screen.name, Modifier.fillMaxWidth().padding(start = 12.dp))
            }
        }
    }
}
