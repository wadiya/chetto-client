package com.wadiya.chetto.client.android.gameplay

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.wadiya.chetto.client.android.*
import com.wadiya.chetto.client.android.R
import com.wadiya.chetto.client.android.gameplay.boardpainters.painter
import com.wadiya.chetto.engine.coordinates.Coord
import com.wadiya.chetto.engine.coordinates.Coord2D
import com.wadiya.chetto.engine.gamemodes.InternalGameMode
import com.wadiya.chetto.engine.models.Move
import com.wadiya.chetto.engine.models.PieceType
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlin.math.*

const val W = 100f // Chess square width before scaling

@Composable
fun <C: Coord<C>> BoardComposable(game: InternalGameMode<C>) {
    val gamePainter = game.painter()

    Box(
        Modifier.fillMaxSize()
            .background(Color.Black)
    )

    val typeToPainter = mapOf(
        PieceType.KING to painterResource(R.drawable.king),
        PieceType.QUEEN to painterResource(R.drawable.queen),
        PieceType.ROOK to painterResource(R.drawable.rook),
        PieceType.BISHOP to painterResource(R.drawable.bishop),
        PieceType.KNIGHT to painterResource(R.drawable.knight),
        PieceType.PAWN to painterResource(R.drawable.pawn),
    )

    val squares = game.board.squares

    val configuration = LocalConfiguration.current
    val width = with(LocalDensity.current) { configuration.screenWidthDp.dp.toPx() }
    val height = with(LocalDensity.current) { configuration.screenHeightDp.dp.toPx() }
    val minZoom = min(width, height) / (W * 8)
    val maxZoom = 5f

    var camPos by remember { mutableStateOf(Offset(W/2, height/4)) }
    var camZoom by remember { mutableStateOf(minZoom) }
    var camRot by remember { mutableStateOf(0f) }
    var targets by remember { mutableStateOf(listOf(camPos, camZoom, camRot)) }
    val state = rememberTransformableState { zoomChange, offsetChange, rotationChange ->
        camZoom *= zoomChange
        camRot += rotationChange
        camPos += offsetChange
    }

    var selected by remember { mutableStateOf<C?>(null) }
    val w = W * camZoom

    fun Offset.toScreen() = Offset(x, 7 - y).rotate(camRot) * w + camPos
    fun Offset.toBoard() = ((this - camPos) / w).rotate(-camRot).let { Offset(it.x, 7 - it.y) }

    val bounds = listOf(
        Offset(-0.5f, -0.5f),
        Offset(board.WIDTH - 0.5f, -0.5f),
        Offset(board.WIDTH - 0.5f, board.HEIGHT - 0.5f),
        Offset(-0.5f, board.HEIGHT - 0.5f)
    )
    val screenBounds = bounds.map { it.toScreen() - camPos }
    val minCamPos = Offset(
        width - screenBounds.map { it.x }.maxOrNull()!!,
        height / 2 - screenBounds.map { it.y }.maxOrNull()!!
    )
    val maxCamPos = Offset(
        max(-screenBounds.map { it.x }.minOrNull()!! + 0.1f, minCamPos.x),
        max( height / 2 - screenBounds.map { it.y }.minOrNull()!!, minCamPos.y)
    )

    Canvas(
        Modifier.fillMaxSize()
            .pointerInput(Unit) {
                coroutineScope {
                    detectTransformGestures { centroid, pan, zoom, rotation ->
                        launch {
                            if (zoom != 1f) {
                                state.zoomBy(zoom)
                                val panScale = (1 - zoom)
                                val c = centroid - camPos
                                state.panBy(Offset(panScale * c.x, panScale * c.y))
                            }
                            state.panBy(pan)
                            if (rotation != 0f) {
                                state.rotateBy(rotation)
                                val c = centroid - camPos
                                state.panBy(c - c.rotate(rotation))
                            }
                            targets = listOf(camPos, camZoom, camRot)
                        }
                    }
                }
            }
            .pointerInput(listOf(camZoom, camPos, camRot)) {
                detectTapGestures { loc ->
                    val point = loc.toBoard()
                    squares
                        .map { it.coord }
                        .firstOrNull { coord ->
                            (coord.toOffset()).let { offset ->
                                (
                                    (offset.x - point.x).pow(2f)
                                    + (offset.y - point.y).pow(2f)
                                ) < 0.25f
                            }
                        }
                        ?.let { clickedCoord ->
                            selected?.let {
                                val move = Move(it, clickedCoord)
                                if (game.isValidMove(move)) {
                                    game.applyMove(move)
                                    selected = null
                                    return@detectTapGestures
                                }
                            }
                            selected = clickedCoord

                        } ?: run {
                        selected = null
                    }
                }
            }
    ) {
        val (visibleSquares, hiddenSquares) = squares.partition { it.coord.let { coord ->
            val center = coord.toOffset().toScreen()
            val r = sqrt(2f) * w / 2
            center.x > -r && center.x < width + r
            && center.y > -r && center.y < height + 3 * r //FIXME Why does '+r' not work here?
        }}

        with (gamePainter) {
            visibleSquares.forEach { square ->
                val center = square.coord.toOffset().toScreen()
                drawSquare(center, square.white, w, camRot)
            }

            visibleSquares.forEach { square ->
                val center = square.coord.toOffset().toScreen()
                if (square.coord == selected) {
                    drawCircle(
                        Color(0.9f, 0.7f, 0f, 0.5f),
                        w / 1.8f,
                        center
                    )
                }
                square.piece?.let { piece ->
                    drawPiece(typeToPainter, center, piece, w)
                }
            }
        }

        // TODO: 14/12/2021 Indicators for off-screen pieces?
//        hiddenSquares.forEach { square ->
//            square.piece?.let { piece ->
//                drawLine(
//                    Color.Red,
//                    Offset(width / 2, height / 2),
//                    square.coord.toOffset().toScreen(),
//                    6f
//                )
//            }
//        }

        selected?.let {
            game.getValidMoves(it).forEach { coord ->
                val center = coord.toOffset().toScreen()
                drawCircle(
                    Color(1f, 0.5f, 0f, 0.8f),
                    w / 6,
                    center
                )
            }
        }
    }

    LaunchedEffect(targets) {
        // TODO: 13/12/2021 Find a way for these animations to happen simultaneously (#2)
        val jobs = mutableListOf<Job>()
        jobs += launch {
            state.animateZoomBy(camZoom.coerceIn(minZoom, maxZoom) / camZoom)
        }
        jobs.joinAll()
        jobs += launch {
            state.animateRotateBy((camRot / 45).roundToInt() * 45f - camRot)
        }
        jobs.joinAll()
        jobs += launch {
            state.animatePanBy(
                Offset(
                    camPos.x.coerceIn(minCamPos.x, maxCamPos.x),
                    camPos.y.coerceIn(minCamPos.y, maxCamPos.y)
                ) - camPos
            )
        }
        jobs.joinAll()
    }
}

fun <C: Coord<C>> C.toOffset(): Offset = when(this) {
    is Coord2D -> Offset(this.x.toFloat(), this.y.toFloat())
    else -> { throw NotImplementedError() }
}

fun Float.toRadians() = this / 360 * 2 * PI

fun Offset.rotate(deg: Float) = deg.toRadians().let { t -> Offset(
    x*cos(t).toFloat() - y*sin(t.toFloat()),
    x*sin(t.toFloat()) + y*cos(t.toFloat())
)}
