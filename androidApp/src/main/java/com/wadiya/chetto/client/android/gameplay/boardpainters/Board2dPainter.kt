package com.wadiya.chetto.client.android.gameplay.boardpainters

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.painter.Painter
import com.wadiya.chetto.engine.boards.Board2D
import com.wadiya.chetto.engine.coordinates.Coord2D
import com.wadiya.chetto.engine.models.Piece
import com.wadiya.chetto.engine.models.PieceType
import com.wadiya.chetto.engine.models.Square

class Board2dPainter(board2D: Board2D): BoardPainter {
    override fun DrawScope.drawSquare(
        center: Offset,
        white: Boolean,
        width: Float,
        rot: Float
    ) {
        val topLeft = center - Offset(width/2, width/2)
        rotate(rot, center) {
            drawRect(
                if (white) { whiteSquareColor } else { blackSquareColor },
                topLeft,
                Size(width, width)
            )
            drawRect(
                Color(0.2f, 0.2f, 0.2f),
                topLeft,
                Size(width, width),
                style = Stroke(width / 32)
            )
            drawRect(
                Color(1f, 1f, 1f, 0.3f),
                topLeft + Offset(width / 32, width / 32),
                Size(width - width / 16, width - width / 16),
                style = Stroke(width / 32)
            )
        }
    }

    override fun DrawScope.drawPiece(
        typeToPainter: Map<PieceType, Painter>,
        center: Offset,
        piece: Piece,
        width: Float
    ) {
        val topLeft = center - Offset(width/2, width/2)
        val padding = width / 16
        translate(
            left = topLeft.x + padding,
            top = topLeft.y + padding
        ) {
            val icon = typeToPainter[piece.type]!!
            val color = when (piece.color) {
                0 -> whitePieceColor
                1 -> blackPieceColor
                else -> Color.Red
            }
            with(icon) {
                draw(Size(width - 2*padding, width - 2*padding), colorFilter = ColorFilter.tint(color))
            }
        }
    }

}