package com.wadiya.chetto.client.android.gameplay.boardpainters

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.painter.Painter
import com.wadiya.chetto.engine.boards.Board2D
import com.wadiya.chetto.engine.gamemodes.ClassicChess
import com.wadiya.chetto.engine.gamemodes.GameMode
import com.wadiya.chetto.engine.models.Piece
import com.wadiya.chetto.engine.models.PieceType

val blackSquareColor = Color(70, 70, 80, 255)
val whiteSquareColor = Color(110, 110, 120, 255)
val blackPieceColor = Color(20, 20, 20, 255)
val whitePieceColor = Color(180, 180, 180, 255)

interface BoardPainter {
    fun DrawScope.drawSquare(
        center: Offset,
        white: Boolean,
        width: Float,
        rot: Float
    )
    fun DrawScope.drawPiece(
        typeToPainter: Map<PieceType, Painter>,
        center: Offset,
        piece: Piece,
        width: Float
    )
}

fun GameMode.painter(): BoardPainter = when(this@painter) {
    is ClassicChess -> Board2dPainter(this@painter.board as Board2D)
    else -> { throw NotImplementedError("Don't know how to draw this gamemode")}
}