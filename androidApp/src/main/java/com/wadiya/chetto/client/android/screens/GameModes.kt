package com.wadiya.chetto.client.android.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.wadiya.chetto.client.android.Game

sealed class GameMode(val name: String, val icon: ImageVector, val color: Color) {
    object Classic : GameMode("Classic", Icons.Filled.Verified, Color.Cyan)
    object Variations : GameMode("Variations", Icons.Filled.Extension, Color.Blue)
    object Arcade : GameMode("Arcade", Icons.Filled.VideogameAsset, Color.Magenta)
    object CoopChallenges : GameMode("Co-op Challenges", Icons.Filled.Groups, Color.Green)
    object BattleRoyale : GameMode("Battle Royale", Icons.Filled.EmojiEvents, Color(1f, 0.7f, 0f))
}

val gameModes = listOf(
    GameMode.Classic,
    GameMode.Variations,
    GameMode.Arcade,
    GameMode.CoopChallenges,
    GameMode.BattleRoyale
)

@Composable
fun GameModes(navController: NavHostController) {
    Column(
        Modifier
            .verticalScroll(rememberScrollState())
            .padding(16.dp)
    ) {
        gameModes.forEach {
            it.ModeCard(navController)
        }
    }
}

@Composable
private fun GameMode.ModeCard(navController: NavHostController) {
    Surface(
        onClick = {
            navController.navigate("gameModes/$name") {
                launchSingleTop = true
            }
        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 8.dp),
        tonalElevation = 1.dp,
        color = MaterialTheme.colorScheme.surface,
        shape = RoundedCornerShape(12.dp),
    ) {
        Column(
            Modifier.padding(12.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                icon,
                "Hello",
                modifier = Modifier
                    .height(80.dp)
                    .width(80.dp),
                colorFilter = ColorFilter.tint(color)
            )
            Text(
                name,
                style = MaterialTheme.typography.headlineLarge,
                color = MaterialTheme.colorScheme.onSurface,
            )
            Text(
                "213 active players",
                style = MaterialTheme.typography.bodySmall,
                color = MaterialTheme.colorScheme.onSurfaceVariant
            )
        }
        Box(Modifier.fillMaxSize()) {
            IconButton(
                onClick = { },
                Modifier.align(Alignment.CenterStart)
            ) {
                Icon(
                    Icons.Filled.HelpOutline,
                    "Gamemode info",
                    tint = MaterialTheme.colorScheme.onSurfaceVariant
                )
            }
        }
    }
}

@Composable
fun GameMode.FindGame() {
    Text(name, style = MaterialTheme.typography.headlineLarge)
}

@ExperimentalMaterialApi
@Composable
fun GameMode.GameView(navController: NavHostController) = Game(navController)
