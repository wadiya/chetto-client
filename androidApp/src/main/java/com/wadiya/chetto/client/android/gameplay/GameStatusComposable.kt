package com.wadiya.chetto.client.android.gameplay

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.statusBarsPadding
import com.wadiya.chetto.engine.gamemodes.GameMode

@Composable
fun GameStatusComposable(game: GameMode) {
    val background = Color.Black.copy(0.8f)
    val screenHeight = LocalConfiguration.current.screenHeightDp.dp
    val state by game.state.collectAsState()
    val gameOver = state is GameMode.State.Complete
    val message = when(state) {
        is GameMode.State.Uninitialized -> "Waiting for first move"
        is GameMode.State.Active -> ""
        is GameMode.State.Complete.Win.Normal -> "Checkmate!"
        is GameMode.State.Complete.Win.Resign -> "Resignation"
        is GameMode.State.Complete.Win.Timeout -> "Timeout"
        is GameMode.State.Complete.Draw.Stalemate -> "Stalemate"
        is GameMode.State.Complete.Draw -> "Draw"
    }
    val color: String = if (game.currentColor == 0) "White" else "Black"

    Box(
        Modifier.fillMaxWidth()
            .background(
                Brush.verticalGradient(
                    listOf(background, Color.Transparent)
                )
            )
            .statusBarsPadding()
            .padding(vertical = 8.dp, horizontal = 16.dp)
            .padding(bottom = 60.dp)
    ) {
        Column(Modifier.fillMaxWidth()) {
            Row {
                Text("Move #${game.moveCount}", color=Color.White)
                Spacer(Modifier.weight(1f))
                Text("$color to move", color=Color.White)
            }
            AnimatedVisibility(state != GameMode.State.Active) {
                Text(
                    text = message,
                    modifier = Modifier.fillMaxWidth()
                        .padding(vertical = if (gameOver) screenHeight / 3 else 16.dp),
                    color = Color.White,
                    style = MaterialTheme.typography.headlineLarge,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}