package com.wadiya.chetto.client.android.screens

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.google.accompanist.placeholder.PlaceholderHighlight
import com.google.accompanist.placeholder.placeholder
import com.google.accompanist.placeholder.shimmer
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState


@ExperimentalFoundationApi
@Composable
fun Store() {
    val placeholder = Modifier.placeholder(
        visible = true,
        color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.3f),
        highlight = PlaceholderHighlight.shimmer(
            MaterialTheme.colorScheme.onBackground.copy(alpha = 0.2f)
        )
    )

    val isRefreshing = rememberSwipeRefreshState(true)

    SwipeRefresh(
        state = isRefreshing,
        onRefresh = { }
    ) {
        LazyVerticalGrid(
            cells = GridCells.Adaptive(120.dp),
            modifier = Modifier.fillMaxSize()
        ) {
            items(1000) { i ->
                Column(
                    Modifier.padding(12.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Surface(
                        placeholder
                            .width(100.dp)
                            .height(100.dp),
                        shape = RoundedCornerShape(12.dp)
                    ) { }
                    Spacer(Modifier.height(8.dp))
                    Text(
                        "",
                        placeholder
                            .width(100.dp),
                    )
                }
            }
        }
    }
}
