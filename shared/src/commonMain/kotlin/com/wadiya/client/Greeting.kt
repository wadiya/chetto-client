package com.wadiya.client

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}